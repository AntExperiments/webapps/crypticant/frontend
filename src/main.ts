import App from './App.svelte';

const app = new App({
	target: document.body,
	props: {
		baseUrl: 'https://sgart.io/api/convert'
	}
});

export default app;